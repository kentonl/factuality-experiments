package edu.uw.cs.lil.events.data;

import org.apache.log4j.Logger;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.trees.TypedDependency;
import edu.uw.cs.lil.utils.data.text.IToken;

public class Token implements IToken {
	public static Logger		log					= Logger
			.getLogger(Token.class);
	private static final long	serialVersionUID	= 7439126432209573078L;
	public final String			dependencyType;
	public final int			governor;
	private final String		after;
	private final String		before;
	private final int			charEnd;
	private final int			charStart;
	private final int			index;
	private final String		tag;
	private final String		text;

	public Token(CoreLabel stanfordToken) {
		this.text = stanfordToken.originalText();
		this.index = stanfordToken.index();
		this.before = stanfordToken.before();
		this.after = stanfordToken.after();
		this.charStart = stanfordToken.beginPosition();
		this.charEnd = stanfordToken.endPosition();
		this.tag = stanfordToken.tag();
		this.governor = -1;
		this.dependencyType = null;
	}

	public Token(IToken other) {
		this.text = other.getText();
		this.index = other.getIndex();
		this.before = other.getBefore();
		this.after = other.getAfter();
		this.charStart = other.getCharStart();
		this.charEnd = other.getCharEnd();
		this.tag = other.getTag();
		this.governor = other.getGovernor();
		this.dependencyType = other.getDependencyType();
	}

	public Token(IToken other, int governor, String dependencyType) {
		this(other, other.getTag(), governor, dependencyType);
	}

	public Token(IToken other, String tag, int governor,
			String dependencyType) {
		this.text = other.getText();
		this.index = other.getIndex();
		this.before = other.getBefore();
		this.after = other.getAfter();
		this.charStart = other.getCharStart();
		this.charEnd = other.getCharEnd();
		this.tag = tag;
		this.governor = governor;
		this.dependencyType = dependencyType;
	}

	public Token(IToken other, TypedDependency dep) {
		this(other, dep.dep().label().tag(), dep.gov().index() - 1,
				dep.reln().toString());
		if (dep.dep().index() - 1 != other.getIndex()) {
			throw new IllegalArgumentException(
					"Dependency is out of order: index of " + dep + " is "
							+ (dep.dep().index() - 1) + ". Expected "
							+ other.getIndex() + " for " + other.getText());
		}
	}

	public Token(String text, int index) {
		this.text = text;
		this.index = index;
		this.before = this.index == 0 ? "" : " ";
		this.after = " ";
		this.charStart = -1;
		this.charEnd = -1;
		this.tag = null;
		this.governor = -1;
		this.dependencyType = null;
	}

	@Override
	public String getAfter() {
		return after;
	}

	@Override
	public String getBefore() {
		return before;
	}

	@Override
	public int getCharEnd() {
		return charEnd;
	}

	@Override
	public int getCharStart() {
		return charStart;
	}

	@Override
	public String getDependencyType() {
		return dependencyType;
	}

	@Override
	public int getGovernor() {
		return governor;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public String getTag() {
		return tag;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}
}
