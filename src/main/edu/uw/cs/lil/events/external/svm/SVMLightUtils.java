package edu.uw.cs.lil.events.external.svm;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.learn.featureset.BiasFeatureSet;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class SVMLightUtils {
	public static final Logger	log	= Logger.getLogger(SVMLightUtils.class);

	private SVMLightUtils() {
	}

	public static ISparseKey[] getFeatureMap(
			List<? extends AbstractSVMSample<?>> samples) {
		return samples.stream().parallel()
				.flatMap(sample -> sample.features.keyStream()).distinct()
				.toArray(ISparseKey[]::new);
	}

	public static Map<ISparseKey, Integer> getInverseFeatureMap(
			ISparseKey[] featureMap) {
		final Map<ISparseKey, Integer> inverseFeatureMap = new HashMap<>();
		for (int i = 0; i < featureMap.length; i++) {
			inverseFeatureMap.put(featureMap[i], i);
		}
		return inverseFeatureMap;
	}

	public static ISparseVector readModel(File modelFile,
			ISparseKey[] featureMap) {
		final IMutableSparseVector weights = new SparseVector();
		try {
			Files.lines(modelFile.toPath())
					.skip(10)
					.map(line -> line.split("#"))
					.map(commentSplit -> commentSplit[0])
					.map(content -> content.split(" "))
					.forEach(
							featureSplit -> {
								final double alpha = Double
										.parseDouble(featureSplit[0]);
								if (featureSplit.length == 1) {
									weights.set(
											ObjectKey.of(BiasFeatureSet.class),
											-alpha);
								} else {
									Arrays.stream(featureSplit)
											.skip(1)
											.map(split -> split.split(":"))
											.forEach(
													av -> weights
															.addToSelf(SparseVector
																	.of(featureMap[Integer
																			.parseInt(av[0]) - 1],
																			alpha
																					* Double.parseDouble(av[1]))));
								}
							});
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		return weights;
	}
}
