package edu.uw.cs.lil.events.learn.model;

import java.util.function.Function;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.learn.featureset.DependencyPathFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.LemmaFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.NGramFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.RootParsePathFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.WordNetEventFeatureSet;
import edu.uw.cs.lil.events.lemma.WordNetLemmatizer;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.LinearBinaryModel;

public class NavyTimeModel extends LinearBinaryModel<EventsToken> {
	public NavyTimeModel(boolean cached) {
		super(cached);
		final Function<EventsToken, String> tagLexicalizer = token -> token
				.getTag();
		final Function<EventsToken, String> rawLexicalizer = token -> token
				.getText().toLowerCase();

		// POS N-grams on target
		addFeatureSet(new NGramFeatureSet(0, 0, tagLexicalizer));
		addFeatureSet(new NGramFeatureSet(-1, 0, tagLexicalizer));
		addFeatureSet(new NGramFeatureSet(-2, 0, tagLexicalizer));

		// N-grams on target
		addFeatureSet(new NGramFeatureSet(0, 0, rawLexicalizer));
		addFeatureSet(new NGramFeatureSet(-1, 0, rawLexicalizer));
		addFeatureSet(new NGramFeatureSet(-2, 0, rawLexicalizer));

		// N-grams before target
		addFeatureSet(new NGramFeatureSet(-1, -1, rawLexicalizer));
		addFeatureSet(new NGramFeatureSet(-2, -1, rawLexicalizer));

		// N-grams after target
		addFeatureSet(new NGramFeatureSet(1, 1, rawLexicalizer));
		addFeatureSet(new NGramFeatureSet(1, 2, rawLexicalizer));

		addFeatureSet(new RootParsePathFeatureSet());
		addFeatureSet(new DependencyPathFeatureSet(1, token -> token.getText()
				.toLowerCase(), 1, false));
		addFeatureSet(new WordNetEventFeatureSet());
		addFeatureSet(new LemmaFeatureSet(new WordNetLemmatizer()));
	}

	public static class Creator implements IResourceCreator<NavyTimeModel> {
		private final String	type;

		public Creator() {
			this("model.navytime");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public NavyTimeModel create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new NavyTimeModel(config.getBoolean("cached"));
		}

		@Override
		public String type() {
			return type;
		}
	}
}
