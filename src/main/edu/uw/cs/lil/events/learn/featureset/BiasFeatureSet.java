package edu.uw.cs.lil.events.learn.featureset;

import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class BiasFeatureSet<V> implements IFeatureSet<V> {
	@Override
	public ISparseVector getFeatures(V dataItem) {
		return SparseVector.of(ObjectKey.of(BiasFeatureSet.class), 1);
	}
}
