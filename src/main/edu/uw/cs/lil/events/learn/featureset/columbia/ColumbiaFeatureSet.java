package edu.uw.cs.lil.events.learn.featureset.columbia;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.learn.featureset.POSFeatureSet;
import edu.uw.cs.lil.events.services.WordNetServices;
import edu.uw.cs.lil.learn.featureset.CompositeFeatureSet;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.DataUtils;

public class ColumbiaFeatureSet extends CompositeFeatureSet<EventsToken> {
	public ColumbiaFeatureSet() {
		super(getFeatureSets());
	}

	private static List<IFeatureSet<EventsToken>> getFeatureSets() {
		final Set<String> perfectSet = DataUtils.setOf("has", "have", "had");
		final Set<String> whSet = DataUtils.setOf("where", "when", "while",
				"who", "why");
		final Set<String> reportingSet = DataUtils.setOf("tell", "accuse",
				"insist", "seem", "believe", "say", "find", "conclude",
				"claim", "trust", "think", "suspect", "doubt", "suppose");

		final List<IFeatureSet<EventsToken>> features = new ArrayList<>();
		features.add(new IsNumericFeatureSet());
		features.add(new POSFeatureSet());
		features.add(new VerbTypeFeatureSet());
		features.add(new ModalTypeFeatureSet());
		features.add(new VBToFeatureSet());
		features.add(new HasChildFeatureSet("perfect", child -> perfectSet
				.contains(child.getText().toLowerCase())));
		features.add(new HasChildFeatureSet("should", child -> child.getText()
				.toLowerCase().equals("should")));
		features.add(new HasChildFeatureSet("wh", child -> whSet.contains(child
				.getText().toLowerCase())));
		features.add(new HasAncestorFeatureSet("reporting",
				ancestor -> reportingSet.contains(WordNetServices
						.getLemma(ancestor.getText().toLowerCase()))));
		features.add(new ParentTagFeatureSet());
		features.add(new ChildAuxTypeFeatureSet());
		features.add(new ChildModalTypeFeatureSet());
		return features;
	}
}
