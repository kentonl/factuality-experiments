package edu.uw.cs.lil.events.learn.featureset;

import java.util.ArrayList;
import java.util.List;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.lemma.WordNetLemmatizer;
import edu.uw.cs.lil.learn.featureset.CompositeFeatureSet;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;

public class BeliefFeatureSet extends CompositeFeatureSet<EventsToken> {
	public BeliefFeatureSet() {
		super(getAllFeatureSets());
	}

	private static List<IFeatureSet<EventsToken>> getAllFeatureSets() {
		final List<IFeatureSet<EventsToken>> eventFeatureSets = getEventFeatureSets();
		return eventFeatureSets;
		/*
		 * return Stream.concat(eventFeatureSets.stream(),
		 * eventFeatureSets.stream().map(GoverningEventFeatureSet::new))
		 * .collect(Collectors.toList());
		 */
	}

	private static List<IFeatureSet<EventsToken>> getEventFeatureSets() {
		final List<IFeatureSet<EventsToken>> eventFeatures = new ArrayList<>();
		eventFeatures.add(new DependencyPathFeatureSet(2, t -> t.getText()
				.toLowerCase(), 2, true));
		eventFeatures.add(new LemmaFeatureSet(new WordNetLemmatizer()));
		eventFeatures.add(new POSFeatureSet());
		return eventFeatures;
	}
}
