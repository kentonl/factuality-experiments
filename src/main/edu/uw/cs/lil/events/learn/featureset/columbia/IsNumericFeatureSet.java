package edu.uw.cs.lil.events.learn.featureset.columbia;

import org.apache.commons.lang.StringUtils;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class IsNumericFeatureSet implements IFeatureSet<EventsToken> {
	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		if (StringUtils.isNumeric(dataItem.getText())) {
			return SparseVector.of(ObjectKey.of(IsNumericFeatureSet.class));
		} else {
			return SparseVector.EMPTY;
		}
	}
}
