package edu.uw.cs.lil.events.learn.model;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.learn.featureset.columbia.ColumbiaFeatureSet;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.LinearContinuousModel;

public class ColumbiaModel extends LinearContinuousModel<EventsToken> {
	public ColumbiaModel(boolean cached) {
		super(cached);
		addFeatureSet(new ColumbiaFeatureSet());
	}

	@Override
	public Double getResult(EventsToken sample) {
		return Math.max(Math.min(super.getResult(sample), 3.0), -3.0);
	}

	public static class Creator implements IResourceCreator<ColumbiaModel> {
		private final String	type;

		public Creator() {
			this("model.columbia");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public ColumbiaModel create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new ColumbiaModel(config.getBoolean("cached"));
		}

		@Override
		public String type() {
			return type;
		}
	}
}
