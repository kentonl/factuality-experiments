package edu.uw.cs.lil.events.learn.featureset;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableList;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public class DependencyPathFeatureSet implements IFeatureSet<EventsToken> {
	public static final Logger					log	= Logger.getLogger(DependencyPathFeatureSet.class);
	private final int							depth;
	private final int							maxLexical;
	private final Function<EventsToken, String>	lexicalizer;
	private final boolean						skip;

	public DependencyPathFeatureSet(int depth,
			Function<EventsToken, String> lexicalizer, int maxLexical,
			boolean skip) {
		this.depth = depth;
		this.lexicalizer = lexicalizer;
		this.maxLexical = maxLexical;
		this.skip = skip;
	}

	private static Stream<Pair<EventsToken, Edge>> edgeStream(EventsToken token) {
		final Stream<Pair<EventsToken, Edge>> childStream = token
				.getState()
				.stream()
				.filter(child -> child.getGovernor() == token.getIndex())
				.map(child -> Pair.of(child,
						Edge.of(null, false, child.getDependencyType())));
		if (token.getGovernor() >= 0) {
			return DataUtils.append(
					childStream,
					Pair.of(token.getState().get(token.getGovernor()),
							Edge.of(null, true, token.getDependencyType())));
		} else {
			return childStream;
		}
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return edgeStream(dataItem)
				.flatMap(edge -> getKeys(edge, depth, dataItem))
				.map(key -> SparseVector.of(key))
				.collect(SparseVector.sumCollector());
	}

	private Stream<Key> getKeys(Pair<EventsToken, Edge> incoming,
			int currentDepth, EventsToken ignoredNeighbor) {
		if (currentDepth <= 0) {
			return Stream.empty();
		}
		final List<Key> currentKeys = lexicalStream(incoming.first()).map(
				lexOption -> Key.of(Edge.of(lexOption, incoming.second())))
				.collect(Collectors.toList());
		final Stream<Key> recursedKeys = edgeStream(incoming.first())
				.filter(outgoing -> outgoing.first() != ignoredNeighbor)
				.flatMap(
						outgoing -> getKeys(outgoing, currentDepth - 1,
								incoming.first()))
				.flatMap(
						remainingKey -> currentKeys.stream().map(
								currentKey -> currentKey
										.withPrependage(remainingKey)));
		return Stream.concat(currentKeys.stream(), recursedKeys).filter(
				key -> key.lexicalCount <= maxLexical);
	}

	private Stream<String> lexicalStream(EventsToken node) {
		if (skip) {
			return Stream.of(lexicalizer.apply(node), null);
		} else {
			return Stream.of(lexicalizer.apply(node));
		}
	}

	public static class Edge implements Serializable {
		private static final long	serialVersionUID	= -3553199200094395282L;
		public final boolean		direction;
		public final String			type;
		public final String			target;

		private Edge(String target, boolean direction, String type) {
			this.target = target;
			this.direction = direction;
			this.type = type;
		}

		public static Edge getStructure(Edge other) {
			if (other.target == null) {
				return other;
			} else {
				return Edge.of("{w}", other.direction, other.type);
			}
		}

		public static Edge of(String target, boolean direction, String type) {
			return new Edge(target, direction, type);
		}

		public static Edge of(String target, Edge other) {
			return new Edge(target, other.direction, other.type);
		}

		@Override
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			if (other instanceof Edge) {
				return Objects.equals(this.target, ((Edge) other).target)
						&& Objects.equals(this.direction,
								((Edge) other).direction)
						&& Objects.equals(this.type, ((Edge) other).type);
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return Objects.hash(target, direction, type);
		}

		@Override
		public String toString() {
			final String targetString = target == null ? "{w}" : target;
			if (direction) {
				return targetString + "-[" + type + "]->";
			} else {
				return targetString + "<-[" + type + "]-";
			}
		}
	}

	public static class Key implements ISparseKey {
		public static final long			serialVersionUID	= -6907585104264455800L;

		private final ImmutableList<Edge>	path;
		private final int					lexicalCount;
		private final int					hashCode;
		private final ImmutableList<Edge>	structure;
		private final ImmutableList<String>	lexicalItems;

		private Key(Stream<Edge> edges) {
			this.path = edges.collect(DataUtils.immutableListCollector());
			this.lexicalCount = path.stream().filter(e -> e.target != null)
					.mapToInt(x -> 1).sum();
			this.hashCode = Objects.hash(path);
			this.structure = path.stream().map(Edge::getStructure)
					.collect(DataUtils.immutableListCollector());
			this.lexicalItems = path.stream().map(e -> e.target)
					.filter(target -> target != null)
					.collect(DataUtils.immutableListCollector());
		}

		public static Key of(Edge e) {
			return new Key(Stream.of(e));
		}

		public static Key of(Key other, Function<Edge, Edge> edgeMapper) {
			return new Key(other.path.stream().map(edgeMapper));
		}

		@Override
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			if (other instanceof Key) {
				return Objects.equals(this.path, ((Key) other).path);
			} else {
				return false;
			}
		}

		public int getLexicalCount() {
			return lexicalCount;
		}

		public List<Edge> getPath() {
			return path;
		}

		@Override
		public Object getStructure() {
			return structure;
		}

		@Override
		public int hashCode() {
			return hashCode;
		}

		@Override
		public int lexicalSize() {
			return lexicalItems.size();
		}

		@Override
		public Stream<String> lexicalStream() {
			return lexicalItems.stream();
		}

		@Override
		public String toString() {
			return path.stream().map(Object::toString)
					.collect(Collectors.joining()).toString()
					+ "{t}";
		}

		public Key withPrependage(Key other) {
			return new Key(Stream.concat(other.path.stream(),
					this.path.stream()));
		}
	}
}
