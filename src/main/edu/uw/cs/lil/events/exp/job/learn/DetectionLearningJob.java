package edu.uw.cs.lil.events.exp.job.learn;

import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.learner.ILearner;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.data.collection.FlatMappedDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class DetectionLearningJob extends
		AbstractLearningJob<EventsToken, Boolean, IMutableSparseVector> {
	public static final Logger log = Logger
			.getLogger(DetectionLearningJob.class);

	private final EventsDataset											dataset;
	private final ILearner<EventsToken, Boolean, IMutableSparseVector>	learner;
	private final IModel<EventsToken, Boolean, IMutableSparseVector>	model;

	public DetectionLearningJob(String id, Set<String> dependencies,
			EventsDataset dataset,
			ILearner<EventsToken, Boolean, IMutableSparseVector> learner,
			IModel<EventsToken, Boolean, IMutableSparseVector> model) {
		super(id, dependencies);
		this.dataset = dataset;
		this.learner = learner;
		this.model = model;
	}

	@Override
	public void run() {
		super.run();
	}

	@Override
	protected IDataCollection<EventsToken> getData() {
		return new FlatMappedDataCollection<>(dataset,
				doc -> doc.stream().flatMap(s -> s.stream()));
	}

	@Override
	protected Boolean getLabel(EventsToken sample) {
		return sample.isEvent();
	}

	@Override
	protected ILearner<EventsToken, Boolean, IMutableSparseVector> getLearner() {
		return learner;
	}

	@Override
	protected IModel<EventsToken, Boolean, IMutableSparseVector> getModel(
			IDataCollection<EventsToken> data) {
		return model;
	}

	public static class Creator
			implements IResourceCreator<DetectionLearningJob> {
		@Override
		public DetectionLearningJob create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new DetectionLearningJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("train")),
					resourceCreatorRepo.create(
							config.configurationAt("learner"), resourceRepo),
					resourceRepo.getResource(config.getString("model")));
		}

		@Override
		public String type() {
			return "job.detection.learn";
		}
	}
}
