package edu.uw.cs.lil.events.exp.job.eval;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.analysis.IAnalysis;
import edu.uw.cs.lil.exp.job.AbstractJob;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;

public abstract class AbstractEvalJob<V, A, P> extends AbstractJob {
	private static final Logger log = Logger.getLogger(AbstractEvalJob.class);

	public AbstractEvalJob(String id, Set<String> dependencies) {
		super(id, dependencies);
	}

	@Override
	public void run() {
		final IDataCollection<V> testPartition = getTestData();
		final IModel<V, A, P> model = getModel();
		log.info(String.format("%d testing samples", testPartition.size()));
		for (final V sample : testPartition) {
			final A prediction = model.getResult(sample);
			for (final IAnalysis<V, A, P> a : getAnalyses()) {
				a.analyze(sample, prediction, getLabel(sample), model);
			}
			sampleProcess(sample);
		}
		postProcess();
	}

	abstract protected List<IAnalysis<V, A, P>> getAnalyses();

	abstract protected A getLabel(V sample);

	abstract protected IModel<V, A, P> getModel();

	abstract protected IDataCollection<V> getTestData();

	abstract protected void postProcess();

	abstract protected void sampleProcess(V sample);
}
