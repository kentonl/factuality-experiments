package edu.uw.cs.lil.events.exp.job.learn;

import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.learner.ILearner;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.data.collection.FlatMappedDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class BeliefLearningJob
		extends AbstractLearningJob<EventsToken, Double, IMutableSparseVector> {
	public static final Logger											log	= Logger
			.getLogger(BeliefLearningJob.class);
	private final EventsDataset											dataset;
	private final ILearner<EventsToken, Double, IMutableSparseVector>	learner;
	private final IModel<EventsToken, Double, IMutableSparseVector>		model;

	public BeliefLearningJob(String id, Set<String> dependencies,
			EventsDataset dataset,
			ILearner<EventsToken, Double, IMutableSparseVector> learner,
			IModel<EventsToken, Double, IMutableSparseVector> model) {
		super(id, dependencies);
		this.dataset = dataset;
		this.learner = learner;
		this.model = model;
	}

	@Override
	protected IDataCollection<EventsToken> getData() {
		return new FlatMappedDataCollection<>(dataset, doc -> doc.stream()
				.flatMap(s -> s.stream()).filter(token -> token.isEvent()));
	}

	@Override
	protected Double getLabel(EventsToken sample) {
		return sample.getFactualityRating().getMean();
	}

	@Override
	protected ILearner<EventsToken, Double, IMutableSparseVector> getLearner() {
		return learner;
	}

	@Override
	protected IModel<EventsToken, Double, IMutableSparseVector> getModel(
			IDataCollection<EventsToken> data) {
		return model;
	}

	public static class Creator implements IResourceCreator<BeliefLearningJob> {
		@Override
		public BeliefLearningJob create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new BeliefLearningJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("train")),
					resourceCreatorRepo.create(
							config.configurationAt("learner"), resourceRepo),
					resourceRepo.getResource(config.getString("model")));
		}

		@Override
		public String type() {
			return "job.belief.learn";
		}
	}
}
