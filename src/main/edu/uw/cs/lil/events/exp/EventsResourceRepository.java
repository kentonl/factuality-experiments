package edu.uw.cs.lil.events.exp;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.events.exp.job.corpus.CorpusBratJob;
import edu.uw.cs.lil.events.exp.job.eval.BeliefEvalJob;
import edu.uw.cs.lil.events.exp.job.eval.DetectionEvalJob;
import edu.uw.cs.lil.events.exp.job.eval.DiscreteBeliefEvalJob;
import edu.uw.cs.lil.events.exp.job.learn.BeliefLearningJob;
import edu.uw.cs.lil.events.exp.job.learn.DetectionLearningJob;
import edu.uw.cs.lil.events.exp.job.learn.DiscreteBeliefLearningJob;
import edu.uw.cs.lil.events.learn.model.BeliefModel;
import edu.uw.cs.lil.events.learn.model.ColumbiaModel;
import edu.uw.cs.lil.events.learn.model.DetectionModel;
import edu.uw.cs.lil.events.learn.model.DiscretizedBeliefModel;
import edu.uw.cs.lil.events.learn.model.NavyTimeModel;
import edu.uw.cs.lil.events.lemma.WordNetLemmatizer;
import edu.uw.cs.lil.events.timeml.reader.TimeMLReader;
import edu.uw.cs.lil.exp.creator.ResourceCreatorRepository;
import edu.uw.cs.lil.exp.creator.StringCreator;
import edu.uw.cs.lil.exp.creator.StringFileCreator;
import edu.uw.cs.lil.exp.job.PrintJob;
import edu.uw.cs.lil.learn.learner.CustomSVMClassificationLearner;
import edu.uw.cs.lil.learn.learner.CustomSVMRegressionLearner;
import edu.uw.cs.lil.learn.learner.SVMClassificationLearner;
import edu.uw.cs.lil.learn.learner.SVMOneVsRestLearner;
import edu.uw.cs.lil.learn.learner.SVMRegressionLearner;

public class EventsResourceRepository extends ResourceCreatorRepository {
	@SuppressWarnings("rawtypes")
	public EventsResourceRepository() {
		// Jobs
		registerResourceCreator(new BeliefEvalJob.Creator());
		registerResourceCreator(new DetectionEvalJob.Creator());
		registerResourceCreator(new DiscreteBeliefEvalJob.Creator());
		registerResourceCreator(new BeliefLearningJob.Creator());
		registerResourceCreator(new DiscreteBeliefLearningJob.Creator());
		registerResourceCreator(new DetectionLearningJob.Creator());
		registerResourceCreator(new PrintJob.Creator());
		registerResourceCreator(new CorpusBratJob.Creator());

		// Misc
		registerResourceCreator(new EventsDataset.Creator());
		registerResourceCreator(new EventsDataset.SubsetCreator());
		registerResourceCreator(new EventsDataset.AnnotationCreator());
		registerResourceCreator(new EventsDataset.CompositeCreator());

		// External
		registerResourceCreator(new WordNetLemmatizer.Creator());

		// Readers
		registerResourceCreator(new TimeMLReader.Creator());

		// Models
		registerResourceCreator(new DetectionModel.Creator());
		registerResourceCreator(new BeliefModel.Creator());
		registerResourceCreator(new NavyTimeModel.Creator());
		registerResourceCreator(new ColumbiaModel.Creator());
		registerResourceCreator(new DiscretizedBeliefModel.Creator());

		// Learners
		registerResourceCreator(
				new SVMClassificationLearner.Creator<EventsToken>());
		registerResourceCreator(
				new SVMRegressionLearner.Creator<EventsToken>());
		registerResourceCreator(
				new SVMOneVsRestLearner.Creator<EventsToken, Integer>());
		registerResourceCreator(
				new CustomSVMClassificationLearner.Creator<EventsToken>());
		registerResourceCreator(
				new CustomSVMRegressionLearner.Creator<EventsToken>());

		// Testing
		registerResourceCreator(new StringCreator());
		registerResourceCreator(new StringFileCreator());
	}
}