package edu.uw.cs.lil.events.lemma;

import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.events.services.WordNetServices;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.utils.DataUtils;

public class WordNetLemmatizer implements ILemmatizer {

	@Override
	public Set<String> lemmatize(String word) {
		return DataUtils.setOf(WordNetServices.getLemma(word));
	}

	@Override
	public Set<String> lemmatize(String word, String tag) {
		return DataUtils.setOf(WordNetServices.getLemma(word, tag));
	}

	public static class Creator implements IResourceCreator<WordNetLemmatizer> {
		private final String	type;

		public Creator() {
			this("lemmatizer.wn");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public WordNetLemmatizer create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new WordNetLemmatizer();
		}

		@Override
		public String type() {
			return type;
		}

	}
}