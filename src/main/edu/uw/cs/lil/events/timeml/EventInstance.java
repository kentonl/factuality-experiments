package edu.uw.cs.lil.events.timeml;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.utils.NumericalCrowdRating;

public class EventInstance extends TemporalInstance {
	private static final long			serialVersionUID	= 2067617112126586209L;
	private final NumericalCrowdRating	factualityRating;
	private final NumericalCrowdRating	eventRating;
	private final String				type;
	private final String				factbankRating;

	public EventInstance(int id, int start, int end, int sentenceIndex,
			String type, NumericalCrowdRating eventRating,
			NumericalCrowdRating factualityRating, String factbankRating) {
		super(id, start, end, sentenceIndex);
		this.type = type;
		this.eventRating = eventRating;
		this.factualityRating = factualityRating;
		this.factbankRating = factbankRating;
	}

	@Override
	public String formatId() {
		return "ei" + getId();
	}

	public NumericalCrowdRating getEventRating() {
		return eventRating;
	}

	public String getFactbankRating() {
		return factbankRating;
	}

	public NumericalCrowdRating getFactualityRating() {
		return factualityRating;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toPrettyString(EventsDocument document) {
		return String.format(
				"%-6s:%-12s(%s)",
				formatId(),
				type,
				document.get(getSentenceIndex()).getExclusiveSpan(getStart(),
						getEnd() + 1));
	}

	@Override
	public String toString() {
		return String.format("%-6s:%-12s[%-2d-%-2d]", formatId(), getType(),
				getStart(), getEnd());
	}
}
