package edu.uw.cs.lil.events.timeml;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;

public class XMLElement {
	private final Map<String, String>	attributes;
	private final int					offset;
	private final String				tag;
	private final StringBuffer			text;

	public XMLElement(String tag, Attributes attributes, int offset) {
		this.attributes = new HashMap<>();
		this.offset = offset;
		this.tag = tag;
		this.text = new StringBuffer();
		for (int i = 0; i < attributes.getLength(); i++) {
			this.attributes.put(attributes.getQName(i), attributes.getValue(i));
		}
	}

	public void appendText(char[] buffer, int start, int length) {
		text.append(buffer, start, length);
	}

	public String getAttribute(String a) {
		return attributes.get(a);
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public int getEndChar() {
		return offset + text.length();
	}

	public int getStartChar() {
		return offset;
	}

	public String getTag() {
		return tag;
	}

	public String getText() {
		return attributes.getOrDefault("text", text.toString());
	}

	@Override
	public String toString() {
		return tag + ": " + attributes + "\n" + text;
	}
}
