package edu.uw.cs.lil.events.timeml;

import java.io.Serializable;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.data.EventsSentence;

public abstract class TimeMLInstance implements Serializable {
	private static final long serialVersionUID = -5532890402344753563L;
	private final int	id;

	public TimeMLInstance(int id) {
		this.id = id;
	}

	public abstract String formatId();

	public int getId() {
		return id;
	}

	public abstract boolean isRelevant(EventsSentence sentence);

	@Override
	public abstract String toString();

	abstract String toPrettyString(EventsDocument document);
}
