package edu.uw.cs.lil.events.timeml;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.data.EventsSentence;

public class TemporalRelationInstance extends TimeMLInstance {
	private static final long serialVersionUID = -6104396916238005758L;
	private final TemporalInstance	arg0;
	private final TemporalInstance	arg1;
	private final String			type;

	public TemporalRelationInstance(int id, String type, TemporalInstance arg0,
			TemporalInstance arg1) {
		super(id);
		this.type = type;
		this.arg0 = arg0;
		this.arg1 = arg1;
	}

	@Override
	public String formatId() {
		return "l" + getId();
	}

	public TemporalInstance getArg0() {
		return arg0;
	}

	public TemporalInstance getArg1() {
		return arg1;
	}

	public String getType() {
		return type;
	}

	public boolean isInterSentence() {
		return !isRelevant(-1)
				&& arg0.getSentenceIndex() != arg1.getSentenceIndex();
	}

	@Override
	public boolean isRelevant(EventsSentence sentence) {
		return isRelevant(sentence.getIndex());
	}

	public boolean isRelevant(int sentenceIndex) {
		return arg0.getSentenceIndex() == sentenceIndex
				|| arg1.getSentenceIndex() == sentenceIndex;
	}

	@Override
	public String toPrettyString(EventsDocument document) {
		return String.format("%-6s:%-35s ---  %-12s---> %-35s%s", formatId(),
				arg0.toPrettyString(document), type,
				arg1.toPrettyString(document), isInterSentence() ? "*" : "");
	}

	@Override
	public String toString() {
		return String.format("%-6s:%-8s ---  %-12s---> %-8s%s", formatId(),
				arg0.formatId(), type, arg1.formatId(), isInterSentence() ? "*"
						: "");
	}
}
