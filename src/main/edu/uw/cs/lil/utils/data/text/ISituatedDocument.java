package edu.uw.cs.lil.utils.data.text;


public interface ISituatedDocument<T extends ISituatedToken<? extends ISentence<T>>, S extends ISituatedSentence<T, ? extends ISituatedDocument<T, S, STATE>>, STATE>
		extends IDocument<T, S> {
	STATE getState();
}