package edu.uw.cs.lil.utils.data.collection;

import java.util.Iterator;
import java.util.stream.Stream;

public class LimitedDataCollection<DI> implements IDataCollection<DI> {
	private final IDataCollection<DI>	data;
	private final int					limit;
	private final int					size;

	public LimitedDataCollection(IDataCollection<DI> data, int limit) {
		this.data = data;
		this.limit = limit;
		this.size = Long.valueOf(stream().count()).intValue();
	}

	@Override
	public Iterator<DI> iterator() {
		return stream().iterator();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Stream<DI> stream() {
		return data.stream().limit(limit);
	}
}