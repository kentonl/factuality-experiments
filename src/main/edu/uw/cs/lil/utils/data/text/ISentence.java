package edu.uw.cs.lil.utils.data.text;

import java.io.Serializable;

import edu.uw.cs.lil.utils.data.collection.IDataCollection;

public interface ISentence<T extends ISituatedToken<? extends ISentence<T>>>
		extends Serializable, IDataCollection<T> {

	T get(int i);

	default String getExclusiveInclusiveSpan(int start, int end) {
		return getExclusiveSpan(start, end)
				+ (end <= 0 ? "" : get(end - 1).getAfter());
	}

	default String getExclusiveSpan(int start, int end) {
		final StringBuffer sb = new StringBuffer();
		stream().filter(token -> token.getIndex() >= start)
				.filter(token -> token.getIndex() < end).forEach(token -> {
					if (token.getIndex() != start) {
						sb.append(token.getBefore());
					}
					sb.append(token.getText());
				});
		return sb.toString();
	}

	default String getInclusiveSpan(int start, int end) {
		return (start == end ? "" : get(start).getBefore())
				+ getExclusiveInclusiveSpan(start, end);
	}

	int getIndex();

	default String getPrefix() {
		if (size() == 0) {
			return null;
		} else {
			return get(0).getBefore();
		}
	}

	default String getSuffix() {
		if (size() == 0) {
			return null;
		} else {
			return get(size() - 1).getAfter();
		}
	}

	default String toOriginalString() {
		return getInclusiveSpan(0, size());
	}
}