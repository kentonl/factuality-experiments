package edu.uw.cs.lil.utils.data.collection;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MappedDataCollection<A, B> implements IDataCollection<B> {
	protected final List<B>	mappedData;

	public MappedDataCollection(IDataCollection<A> data,
			Function<A, B> dataMapper) {
		this.mappedData = data.stream().map(dataMapper)
				.collect(Collectors.toList());
	}

	@Override
	public Iterator<B> iterator() {
		return mappedData.iterator();
	}

	@Override
	public int size() {
		return mappedData.size();
	}

	@Override
	public Stream<B> stream() {
		return mappedData.stream();
	}
}