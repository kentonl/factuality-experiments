package edu.uw.cs.lil.utils.data;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class ConcurrentDefaultHashMap<K, V> extends ConcurrentHashMap<K, V> {
	private static final long serialVersionUID = -8917336853806709595L;
	private final Function<K, V>	defaultMapper;

	public ConcurrentDefaultHashMap(Function<K, V> defaultMapper) {
		this.defaultMapper = defaultMapper;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		return computeIfAbsent((K) key, defaultMapper);
	}
}
