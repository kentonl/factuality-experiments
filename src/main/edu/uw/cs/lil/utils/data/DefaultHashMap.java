package edu.uw.cs.lil.utils.data;

import java.util.HashMap;
import java.util.function.Function;

public class DefaultHashMap<K, V> extends HashMap<K, V> {
	private static final long serialVersionUID = 2674655225353775071L;
	private final Function<K, V>	defaultMapper;

	public DefaultHashMap(Function<K, V> defaultMapper) {
		this.defaultMapper = defaultMapper;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		return computeIfAbsent((K) key, defaultMapper);
	}
}
