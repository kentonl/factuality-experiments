package edu.uw.cs.lil.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.MinMaxPriorityQueue;

import edu.uw.cs.lil.utils.data.collection.FilteredDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public class DataUtils {
	public static Logger	log	= Logger.getLogger(DataUtils.class);

	private DataUtils() {
	}

	public static <A> Stream<A> append(Stream<A> s, A newElement) {
		return Stream.concat(s, Stream.of(newElement));
	}

	public static <A> A argmax(A x, A y, Function<A, Double> mapper) {
		return mapper.apply(x) > mapper.apply(y) ? x : y;
	}

	public static <T, C extends Comparable<C>> Collector<T, ?, Optional<T>> argmaxCollector(
			Function<T, C> comparableMapper) {
		return Collectors.maxBy((x, y) -> comparableMapper.apply(x).compareTo(
				comparableMapper.apply(y)));
	}

	public static <A> A argmin(A x, A y, Function<A, Double> mapper) {
		return mapper.apply(x) > mapper.apply(y) ? y : x;
	}

	public static <T, C extends Comparable<C>> Collector<T, ?, Optional<T>> argminCollector(
			Function<T, C> comparableMapper) {
		return Collectors.minBy((x, y) -> comparableMapper.apply(x).compareTo(
				comparableMapper.apply(y)));
	}

	public static <K, V> Collector<K, ?, Map<K, V>> cachedFunctionCollector(
			Function<K, V> f) {
		return Collectors.toMap(k -> k, k -> f.apply(k));
	}

	public static <K, V> Map<List<K>, V> crossProduct(Map<List<K>, V> m1,
			Map<List<K>, V> m2, BinaryOperator<V> op) {
		final Map<List<K>, V> output = new HashMap<>();
		m1.forEach((l1, v1) -> m2.forEach((l2, v2) -> output.put(
				Stream.concat(l1.stream(), l2.stream()).collect(
						Collectors.toList()), op.apply(v1, v2))));
		return output;
	}

	public static <K, V> Collector<Map<K, V>, ?, Map<List<K>, V>> crossProductCollector(
			V initial, BinaryOperator<V> op) {
		return Collectors.<Map<K, V>, Map<List<K>, V>> reducing(
				toMap(Collections.<K> emptyList(), initial),
				m -> listifyKeys(m), (m1, m2) -> crossProduct(m1, m2, op));
	}

	public static <DI> Collector<DI, ?, IDataCollection<DI>> dataCollector() {
		return Collectors.collectingAndThen(Collectors.toList(),
				l -> listToDataCollection(l));
	}

	public static <T> IDataCollection<T> emptyCollection() {
		return listToDataCollection(Collections.emptyList());
	}

	public static <A> Stream<Pair<Integer, A>> enumerate(Stream<A> a) {
		return zip(IntStream.iterate(0, i -> i + 1).boxed(), a,
				(i, x) -> Pair.<Integer, A> of(i, x));
	}

	public static <T> Collector<T, ?, ImmutableList<T>> immutableListCollector() {
		return new Collector<T, ImmutableList.Builder<T>, ImmutableList<T>>() {
			@Override
			public BiConsumer<ImmutableList.Builder<T>, T> accumulator() {
				return (b, t) -> b.add(t);
			}

			@Override
			public Set<Characteristics> characteristics() {
				return Collections.emptySet();
			}

			@Override
			public BinaryOperator<ImmutableList.Builder<T>> combiner() {
				return (left, right) -> {
					left.addAll(right.build());
					return left;
				};
			}

			@Override
			public Function<ImmutableList.Builder<T>, ImmutableList<T>> finisher() {
				return ImmutableList.Builder::build;
			}

			@Override
			public Supplier<ImmutableList.Builder<T>> supplier() {
				return ImmutableList::builder;
			}
		};
	}

	public static List<String> jsonToList(JSONArray a) {
		final List<String> output = new ArrayList<>();
		try {
			for (int i = 0; i < a.length(); i++) {
				output.add(a.getString(i));
			}
		} catch (final JSONException e) {
			throw new RuntimeException(e);
		}
		return output;
	}

	public static <K, V> K keyWithMaxValue(Map<K, V> m, Comparator<V> comp) {
		return m.entrySet().stream()
				.max((e1, e2) -> comp.compare(e1.getValue(), e2.getValue()))
				.map(entry -> entry.getKey()).get();
	}

	public static <K, V> Map<List<K>, V> listifyKeys(Map<K, V> m) {
		return m.entrySet()
				.stream()
				.collect(
						Collectors.<Map.Entry<K, V>, List<K>, V> toMap(
								entry -> Arrays.asList(entry.getKey()),
								entry -> entry.getValue()));
	}

	public static <DI> IDataCollection<DI> listToDataCollection(List<DI> l) {
		return new IDataCollection<DI>() {
			@Override
			public int size() {
				return l.size();
			}

			@Override
			public Stream<DI> stream() {
				return l.stream();
			}
		};
	}

	public static <X, Y, Z> Map<Z, Y> mapKeyToMap(Map<X, Y> inputMap,
			Function<X, Z> m) {
		return inputMap
				.entrySet()
				.stream()
				.collect(
						Collectors.toMap(entry -> m.apply(entry.getKey()),
								entry -> entry.getValue()));
	}

	public static <X, Y, Z> Map<X, Z> mapToMap(Map<X, Y> inputMap,
			BiFunction<X, Y, Z> m) {
		return inputMap
				.entrySet()
				.stream()
				.collect(
						Collectors.toMap(
								entry -> entry.getKey(),
								entry -> m.apply(entry.getKey(),
										entry.getValue())));
	}

	public static <X, Y, Z> Map<X, Z> mapToMap(Map<X, Y> inputMap,
			Function<Y, Z> m) {
		return mapToMap(inputMap, m, false);
	}

	public static <X, Y, Z> Map<X, Z> mapToMap(Map<X, Y> inputMap,
			Function<Y, Z> m, boolean parallel) {
		if (parallel) {
			return inputMap
					.entrySet()
					.parallelStream()
					.collect(
							Collectors.toMap(entry -> entry.getKey(),
									entry -> m.apply(entry.getValue())));
		} else {
			return inputMap
					.entrySet()
					.stream()
					.collect(
							Collectors.toMap(entry -> entry.getKey(),
									entry -> m.apply(entry.getValue())));
		}
	}

	public static <K, V> Collector<Map<K, V>, ?, Map<K, V>> mergeCollector(
			BinaryOperator<V> op) {
		return new Collector<Map<K, V>, Map<K, V>, Map<K, V>>() {
			@Override
			public final BiConsumer<Map<K, V>, Map<K, V>> accumulator() {
				return (x, y) -> y.forEach((k, v) -> x.merge(k, v, op));
			}

			@Override
			public Set<Characteristics> characteristics() {
				return DataUtils.setOf(Characteristics.IDENTITY_FINISH,
						Characteristics.UNORDERED);
			}

			@Override
			public BinaryOperator<Map<K, V>> combiner() {
				return (x, y) -> {
					if (x.size() > y.size()) {
						y.forEach((k, v) -> x.merge(k, v, op));
						return x;
					} else {
						x.forEach((k, v) -> y.merge(k, v, op));
						return y;
					}
				};
			}

			@Override
			public Function<Map<K, V>, Map<K, V>> finisher() {
				return x -> x;
			}

			@Override
			public Supplier<Map<K, V>> supplier() {
				return () -> new HashMap<>();
			}
		};
	}

	public static <K, V> Map<K, V> mergeMaps(Map<K, V> m1, Map<K, V> m2,
			BinaryOperator<V> op) {
		return mergeMaps(m1, m2, x -> x, op);
	}

	public static <K, V, W> Map<K, V> mergeMaps(Map<K, V> m1, Map<K, W> m2,
			Function<W, V> mapper, BinaryOperator<V> op) {
		final Map<K, V> output = new HashMap<>(m1);
		m2.forEach((k, w) -> output.merge(k, mapper.apply(w), op));
		return output;
	}

	public static <DI> List<IDataCollection<DI>> partition(
			IDataCollection<DI> data, int splits) {
		return partition(data, splits, x -> x);
	}

	public static <DI> List<IDataCollection<DI>> partition(
			IDataCollection<DI> data, int splits, Function<DI, Object> grouping) {
		return IntStream
				.range(0, splits)
				.mapToObj(
						k -> new FilteredDataCollection<>(data,
								x -> Math.abs(grouping.apply(x).hashCode())
										% splits == k))
				.collect(Collectors.toList());
	}

	public static <T> Collector<T, ?, List<T>> reservoirSamplingCollector(int k) {
		return reservoirSamplingCollector(k, new Random());
	}

	public static <T> Collector<T, ?, List<T>> reservoirSamplingCollector(
			int k, Random rand) {
		return new Collector<T, MinMaxPriorityQueue<Pair<Double, T>>, List<T>>() {
			@Override
			public BiConsumer<MinMaxPriorityQueue<Pair<Double, T>>, T> accumulator() {
				return (minQueue, sample) -> {
					minQueue.add(Pair.of(rand.nextDouble(), sample));
				};
			}

			@Override
			public Set<Characteristics> characteristics() {
				return EnumSet.of(Characteristics.UNORDERED);
			}

			@Override
			public BinaryOperator<MinMaxPriorityQueue<Pair<Double, T>>> combiner() {
				return (left, right) -> {
					left.addAll(right);
					return left;
				};
			}

			@Override
			public Function<MinMaxPriorityQueue<Pair<Double, T>>, List<T>> finisher() {
				return minQueue -> minQueue.stream().map(Pair::second)
						.collect(Collectors.toList());
			}

			@Override
			public Supplier<MinMaxPriorityQueue<Pair<Double, T>>> supplier() {
				return () -> MinMaxPriorityQueue
						.<Pair<Double, T>> orderedBy(
								(x, y) -> Double.compare(x.first(), y.first()))
						.maximumSize(k).create();
			}
		};
	}

	public static <T> IDataCollection<T> reverse(IDataCollection<T> data) {
		final List<T> list = data.stream().collect(Collectors.toList());
		Collections.reverse(list);
		return listToDataCollection(list);
	}

	@SafeVarargs
	public static <T> Set<T> setOf(T... elements) {
		return Arrays.stream(elements).collect(Collectors.toSet());
	}

	public static <T> Stream<T> shuffle(Stream<T> data) {
		final List<T> output = data.collect(Collectors.toList());
		Collections.shuffle(output);
		return output.stream();
	}

	public static <DI> Stream<DI> stream(Iterable<DI> iterable) {
		return StreamSupport.stream(iterable.spliterator(), false);
	}

	public static <DI> Stream<DI> stream(Iterator<DI> iterator, int size) {
		return StreamSupport.stream(
				Spliterators.spliterator(iterator, size, 0), false);
	}

	public static <DI> Stream<DI> stream(Supplier<DI> supplier, int size) {
		return stream(new Iterator<DI>() {
			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public DI next() {
				return supplier.get();
			}
		}, size).limit(size);
	}

	public static <K, V> Map<K, V> toMap(K key, V value) {
		final Map<K, V> output = new HashMap<>();
		output.put(key, value);
		return output;
	}

	public static <T, C extends Comparable<C>> Collector<T, ?, List<T>> topKCollector(
			Function<T, C> comparableMapper, int k) {
		return new Collector<T, MinMaxPriorityQueue<T>, List<T>>() {
			@Override
			public BiConsumer<MinMaxPriorityQueue<T>, T> accumulator() {
				return (pq, x) -> pq.add(x);
			}

			@Override
			public Set<Characteristics> characteristics() {
				return DataUtils.setOf(Characteristics.UNORDERED);
			}

			@Override
			public BinaryOperator<MinMaxPriorityQueue<T>> combiner() {
				return (left, right) -> {
					left.addAll(right);
					return left;
				};
			}

			@Override
			public Function<MinMaxPriorityQueue<T>, List<T>> finisher() {
				return pq -> {
					final List<T> output = new ArrayList<>(k);
					for (int i = 0; i < k; i++) {
						output.add(pq.remove());
					}
					return output;
				};
			}

			@Override
			public Supplier<MinMaxPriorityQueue<T>> supplier() {
				return () -> MinMaxPriorityQueue
						.<T> orderedBy(
								(x, y) -> comparableMapper.apply(y).compareTo(
										comparableMapper.apply(x)))
						.expectedSize(k).maximumSize(k).create();
			}
		};
	}

	public static <A, B, C> Stream<C> zip(Stream<? extends A> a,
			Stream<? extends B> b,
			BiFunction<? super A, ? super B, ? extends C> zipper) {
		Objects.requireNonNull(zipper);
		@SuppressWarnings("unchecked")
		final Spliterator<A> aSpliterator = (Spliterator<A>) Objects
				.requireNonNull(a).spliterator();
		@SuppressWarnings("unchecked")
		final Spliterator<B> bSpliterator = (Spliterator<B>) Objects
				.requireNonNull(b).spliterator();

		// Zipping looses DISTINCT and SORTED characteristics
		final int both = aSpliterator.characteristics()
				& bSpliterator.characteristics()
				& ~(Spliterator.DISTINCT | Spliterator.SORTED);
		final int characteristics = both;

		final long zipSize = (characteristics & Spliterator.SIZED) != 0 ? Math
				.min(aSpliterator.getExactSizeIfKnown(),
						bSpliterator.getExactSizeIfKnown()) : -1;

		final Iterator<A> aIterator = Spliterators.iterator(aSpliterator);
		final Iterator<B> bIterator = Spliterators.iterator(bSpliterator);
		final Iterator<C> cIterator = new Iterator<C>() {
			@Override
			public boolean hasNext() {
				return aIterator.hasNext() && bIterator.hasNext();
			}

			@Override
			public C next() {
				return zipper.apply(aIterator.next(), bIterator.next());
			}
		};

		final Spliterator<C> split = Spliterators.spliterator(cIterator,
				zipSize, characteristics);
		return a.isParallel() || b.isParallel() ? StreamSupport.stream(split,
				true) : StreamSupport.stream(split, false);
	}
}
