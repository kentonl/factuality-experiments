package edu.uw.cs.lil.utils.vector;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.key.CompositeKey;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;
import gnu.trove.iterator.TObjectDoubleIterator;
import gnu.trove.map.hash.TObjectDoubleHashMap;

public class SparseVector implements IMutableSparseVector {
	public final static ISparseVector				EMPTY				= new SparseVector();
	private static final long						serialVersionUID	= -7610098719906004236L;
	private final TObjectDoubleHashMap<ISparseKey>	map;

	public SparseVector() {
		this.map = new TObjectDoubleHashMap<>();
	}

	public static SparseVector of(ISparseKey key) {
		return SparseVector.of(key, 1.0);
	}

	public static SparseVector of(ISparseKey key, double value) {
		return SparseVector.of(Pair.of(key, value));
	}

	public static SparseVector of(ISparseVector other) {
		return SparseVector.of(other.stream());
	}

	@SafeVarargs
	public static SparseVector of(Pair<ISparseKey, Double>... pairs) {
		return SparseVector.of(Arrays.stream(pairs));
	}

	public static SparseVector of(Stream<Pair<ISparseKey, Double>> pairs) {
		final SparseVector v = new SparseVector();
		pairs.forEach(p -> v.map.put(p.first(), p.second()));
		return v;
	}

	public static Collector<ISparseVector, ?, ISparseVector> sumCollector() {
		return new Collector<ISparseVector, SparseVector, ISparseVector>() {
			@Override
			public final BiConsumer<SparseVector, ISparseVector> accumulator() {
				return (x, y) -> x.addToSelf(y);
			}

			@Override
			public Set<Characteristics> characteristics() {
				return DataUtils.setOf(Characteristics.UNORDERED,
						Characteristics.IDENTITY_FINISH);
			}

			@Override
			public BinaryOperator<SparseVector> combiner() {
				return (x, y) -> {
					if (x.size() > y.size()) {
						x.addToSelf(y);
						return x;
					} else {
						y.addToSelf(x);
						return y;
					}
				};
			}

			@Override
			public Function<SparseVector, ISparseVector> finisher() {
				return x -> x;
			}

			@Override
			public Supplier<SparseVector> supplier() {
				return () -> new SparseVector();
			}
		};
	}

	public static <T> Collector<T, ?, ISparseVector> weightedSumCollector(
			Function<T, Double> weightFunction,
			Function<T, SparseVector> vectorFunction) {
		return new Collector<T, SparseVector, ISparseVector>() {
			@Override
			public BiConsumer<SparseVector, T> accumulator() {
				return (x, t) -> x.addToSelf(
						vectorFunction.apply(t).times(weightFunction.apply(t)));
			}

			@Override
			public Set<Characteristics> characteristics() {
				return DataUtils.setOf(Characteristics.UNORDERED,
						Characteristics.IDENTITY_FINISH);
			}

			@Override
			public BinaryOperator<SparseVector> combiner() {
				return (x, y) -> {
					if (x.size() > y.size()) {
						x.addToSelf(y);
						return x;
					} else {
						y.addToSelf(x);
						return y;
					}
				};
			}

			@Override
			public Function<SparseVector, ISparseVector> finisher() {
				return x -> x;
			}

			@Override
			public Supplier<SparseVector> supplier() {
				return () -> new SparseVector();
			}
		};
	}

	@Override
	public ISparseVector add(ISparseVector other) {
		if (this.size() < other.size()) {
			return other.add(this);
		} else {
			final SparseVector result = SparseVector.of(this);
			result.addToSelf(other);
			return result;
		}
	}

	@Override
	public ISparseVector addTimes(double coefficient, ISparseVector other) {
		final SparseVector result = SparseVector.of(this);
		result.addTimesToSelf(coefficient, other);
		return result;
	}

	@Override
	public void addTimesToSelf(double coefficient, ISparseVector other) {
		other.forEach((key, value) -> map.adjustOrPutValue(key,
				coefficient * value, coefficient * value));
	}

	@Override
	public void addToSelf(ISparseVector other) {
		other.forEach((key, value) -> map.adjustOrPutValue(key, value, value));
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public ISparseVector crossProduct(ISparseVector other) {
		return this.stream()
				.<SparseVector> flatMap(p1 -> other.stream()
						.map(p2 -> SparseVector.of(
								CompositeKey.of(p1.first(), p2.first()),
								p1.second() * p2.second())))
				.collect(sumCollector());
	}

	@Override
	public double dotProduct(ISparseVector other) {
		if (this.size() < other.size()) {
			return other.dotProduct(this);
		} else {
			return other.stream()
					.mapToDouble(p -> this.get(p.first()) * p.second()).sum();
		}
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof SparseVector) {
			return Objects.equals(this.map, ((SparseVector) other).map);
		} else {
			return false;
		}
	}

	@Override
	public void forEach(BiConsumer<ISparseKey, Double> f) {
		map.forEachEntry((key, value) -> {
			f.accept(key, value);
			return true;
		});
	}

	@Override
	public double get(ISparseKey key) {
		return map.get(key);
	}

	@Override
	public Set<ISparseKey> getKeys() {
		return map.keySet();
	}

	@Override
	public int hashCode() {
		return Objects.hash(map);
	}

	@Override
	public Stream<ISparseKey> keyStream() {
		return stream().map(entry -> entry.first());
	}

	@Override
	public SparseVector mapEntries(
			BiFunction<ISparseKey, Double, ISparseKey> keyFunction,
			BiFunction<ISparseKey, Double, Double> valueFunction) {
		final SparseVector result = new SparseVector();
		this.forEach(
				(key, value) -> result.map.put(keyFunction.apply(key, value),
						valueFunction.apply(key, value)));
		return result;
	}

	@Override
	public void set(ISparseKey key, double value) {
		map.put(key, value);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Stream<Pair<ISparseKey, Double>> stream() {
		final TObjectDoubleIterator<ISparseKey> iterator = map.iterator();
		return DataUtils.stream(() -> {
			iterator.advance();
			return Pair.of(iterator.key(), iterator.value());
		} , map.size());
	}

	@Override
	public SparseVector times(double coefficient) {
		final SparseVector result = SparseVector.of(this);
		result.timesSelf(coefficient);
		return result;
	}

	@Override
	public void timesSelf(double coefficient) {
		map.transformValues(x -> x * coefficient);
	}

	@Override
	public String toString() {
		return map.toString();
	}
}
