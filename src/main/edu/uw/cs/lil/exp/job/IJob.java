package edu.uw.cs.lil.exp.job;

import java.util.Set;

public interface IJob extends Runnable {
	public Set<String> getDependencies();

	public String getId();
}
