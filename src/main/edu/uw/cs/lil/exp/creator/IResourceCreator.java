package edu.uw.cs.lil.exp.creator;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.exp.repository.IResourceRepository;

public interface IResourceCreator<T> {
	T create(HierarchicalConfiguration config,
			IResourceRepository resourceRepo,
			IResourceCreatorRepository resourceCreatorRepo);

	String type();
}
