package edu.uw.cs.lil.exp.creator;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.exp.repository.IResourceRepository;

public class StringCreator implements IResourceCreator<String> {
	@Override
	public String create(HierarchicalConfiguration config,
			IResourceRepository resourceRepo,
			IResourceCreatorRepository resourceCreatorRepo) {
		return config.getString("data");
	}

	@Override
	public String type() {
		return "string";
	}
}
