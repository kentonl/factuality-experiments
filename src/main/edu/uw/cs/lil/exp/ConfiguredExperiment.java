package edu.uw.cs.lil.exp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import com.google.common.io.Files;

import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.job.IJob;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.exp.repository.ResourceRepository;
import edu.uw.cs.lil.utils.ConfigUtils;

public class ConfiguredExperiment implements Runnable {
	private static final PatternLayout			defaultLogLayout	= new PatternLayout(
																			"%-5p [%c{1}]: %m%n");
	private static final Level					defaultLogLevel		= Level.INFO;
	private static final Logger					log					= Logger.getLogger(ConfiguredExperiment.class);
	private final XMLConfiguration				configuration;
	private final String						experimentName;
	private final FileAppender					jobAppender;
	private final List<IJob>					jobs				= new LinkedList<>();
	private final Layout						logLayout;
	private final IResourceCreatorRepository	creatorRepo;
	private final IResourceRepository			resourceRepo		= new ResourceRepository();

	public ConfiguredExperiment(File configFile,
			IResourceCreatorRepository resourceCreatorRepo) throws IOException,
			ConfigurationException {
		Logger.getRootLogger().removeAllAppenders();
		Logger.getRootLogger().setLevel(defaultLogLevel);
		if (!configFile.exists()) {
			throw new IllegalArgumentException("Experiments file not found: "
					+ configFile);
		}
		this.experimentName = Files.getNameWithoutExtension(configFile
				.toString());
		this.configuration = new XMLConfiguration(configFile);
		this.configuration.setThrowExceptionOnMissing(true);
		this.creatorRepo = resourceCreatorRepo;

		logLayout = ConfigUtils.getOptionalString("logLayout", configuration)
				.map(PatternLayout::new).orElse(defaultLogLayout);
		Logger.getRootLogger().addAppender(new ConsoleAppender(logLayout));

		jobAppender = new FileAppender(logLayout, getLogFilename("init"), false);
		jobAppender.setAppend(false);
		Logger.getRootLogger().addAppender(jobAppender);

		Logger.getRootLogger().setLevel(
				ConfigUtils.getOptionalString("logLevel", configuration)
						.map(Level::toLevel).orElse(defaultLogLevel));
		log.info("Initalized experiment.");
	}

	public static List<IJob> topologicalSort(List<IJob> jobs) {
		final Map<IJob, List<IJob>> dependents = jobs.stream().collect(
				Collectors.toMap(
						j -> j,
						j -> jobs
								.stream()
								.filter(k -> k.getDependencies().contains(
										j.getId()))
								.collect(Collectors.toList())));
		final LinkedList<IJob> sortedJobs = new LinkedList<>();
		final Set<IJob> visitedJobs = new HashSet<>();
		final Set<IJob> exploringJobs = new HashSet<>();
		while (sortedJobs.size() < jobs.size()) {
			visit(jobs.stream().filter(j -> !visitedJobs.contains(j))
					.findFirst().get(), visitedJobs, exploringJobs, dependents,
					sortedJobs);
		}
		return sortedJobs;
	}

	private static void visit(IJob job, Set<IJob> visitedJobs,
			Set<IJob> exploringJobs, Map<IJob, List<IJob>> dependents,
			LinkedList<IJob> sortedJobs) {
		if (exploringJobs.contains(job)) {
			throw new IllegalArgumentException(
					"Cycles exist in dependencies: (see " + job.getId() + ")");
		} else if (!visitedJobs.contains(job)) {
			exploringJobs.add(job);
			for (final IJob d : dependents.get(job)) {
				visit(d, visitedJobs, exploringJobs, dependents, sortedJobs);
			}
			exploringJobs.remove(job);
			visitedJobs.add(job);
			sortedJobs.addFirst(job);
		}
	}

	@Override
	public void run() {
		try {
			log.info("Loading resources...");
			try {
				loadResources(configuration);
			} catch (final ConfigurationException e) {
				throw new RuntimeException(e);
			}
			loadJobs(configuration);
			log.info("Done loading resources.");
		} catch (final Exception e) {
			log.error("Exception thrown while loading resources.", e);
			throw e;
		}

		final StopWatch stopwatch = new StopWatch();
		for (final IJob j : topologicalSort(jobs)) {
			jobAppender.setFile(getLogFilename(j.getId()));
			jobAppender.activateOptions();
			stopwatch.start();
			log.info("Starting job: " + j.getId());
			try {
				j.run();
			} catch (final Exception e) {
				log.error("Exception thrown while running job: " + j.getId(), e);
				throw e;
			}
			stopwatch.stop();
			log.info("Job " + j.getId() + " finished in " + stopwatch);
			stopwatch.reset();
		}
	}

	private String getLogFilename(String prefix) {
		return Paths.get("logs", experimentName, prefix.concat(".log"))
				.toString();
	}

	private void loadJobs(XMLConfiguration config) {
		jobs.addAll(config
				.configurationsAt("jobs.job")
				.stream()
				.map(jobConfig -> (IJob) creatorRepo.create(jobConfig,
						resourceRepo)).collect(Collectors.toList()));
		for (final IJob j : jobs) {
			resourceRepo.put(j.getId(), j);
		}
	}

	private void loadResources(XMLConfiguration config)
			throws ConfigurationException {
		for (final String includeFile : config.getStringArray("include")) {
			loadResources(new XMLConfiguration(includeFile));
		}
		for (final HierarchicalConfiguration resourceConfig : config
				.configurationsAt("resources.resource")) {
			resourceRepo.put(resourceConfig.getString("id"),
					creatorRepo.create(resourceConfig, resourceRepo));
		}
	}
}
