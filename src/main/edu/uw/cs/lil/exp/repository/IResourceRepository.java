package edu.uw.cs.lil.exp.repository;

public interface IResourceRepository {
	<T> T getResource(String id);

	void put(String id, Object resource);
}
