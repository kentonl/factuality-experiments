package edu.uw.cs.lil.learn.learner;

import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public interface ILinearLearner<V, A> extends
		ILearner<V, A, IMutableSparseVector> {
	// Nothing
}
