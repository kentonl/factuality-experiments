package edu.uw.cs.lil.learn.learner;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class SVMOneVsRestLearner<V, A> implements
		ILearner<V, A, Map<A, IModel<V, Boolean, IMutableSparseVector>>> {
	public static final Logger	log	= Logger
			.getLogger(SVMOneVsRestLearner.class);
	private final double		cost;

	public SVMOneVsRestLearner(double cost) {
		this.cost = cost;
	}

	@Override
	public void train(
			IModel<V, A, Map<A, IModel<V, Boolean, IMutableSparseVector>>> model,
			IDataCollection<V> data, Function<V, A> labeler,
			ToDoubleFunction<V> varianceLabeler) {
		final Set<A> labels = data.stream().map(labeler)
				.collect(Collectors.toSet());
		labels.stream().parallel()
				.forEach(a -> new SVMClassificationLearner<V>(cost).train(
						model.getParams().get(a), data,
						sample -> labeler.apply(sample) == a));
	}

	public static class Creator<V, A>
			implements IResourceCreator<SVMOneVsRestLearner<V, A>> {
		@Override
		public SVMOneVsRestLearner<V, A> create(
				HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new SVMOneVsRestLearner<>(config.getDouble("cost"));
		}

		@Override
		public String type() {
			return "learner.svm.classification.multi";
		}
	}
}
