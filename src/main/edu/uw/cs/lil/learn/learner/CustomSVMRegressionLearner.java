package edu.uw.cs.lil.learn.learner;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.learn.featureset.BiasFeatureSet;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class CustomSVMRegressionLearner<V> implements ILinearLearner<V, Double> {
	public static final Logger	log	= Logger.getLogger(CustomSVMRegressionLearner.class);
	private final double		c;
	private final double		epsilon;
	private final boolean		useVariance;

	public CustomSVMRegressionLearner(double c, double epsilon,
			boolean useVariance) {
		this.c = c;
		this.epsilon = epsilon;
		this.useVariance = useVariance;
	}

	@Override
	public void train(IModel<V, Double, IMutableSparseVector> model,
			IDataCollection<V> samples, Function<V, Double> labeler,
			ToDoubleFunction<V> varianceLabeler) {
		if (samples.size() == 0) {
			return;
		}
		log.debug(String.format("Beginning training over %d samples",
				samples.size()));

		double s = 0;

		if (useVariance && varianceLabeler != null) {
			s = epsilon
					/ samples.stream().mapToDouble(varianceLabeler).average()
							.getAsDouble();
			log.info("s=" + s);
		}

		final ArrayList<V> data = samples.stream().collect(
				Collectors.toCollection(ArrayList::new));
		final ArrayList<ISparseVector> features = data.stream().parallel()
				.map(sample -> model.getFeatures(sample))
				.collect(Collectors.toCollection(ArrayList::new));

		final ISparseKey[] featureMap = features.stream()
				.flatMap(ISparseVector::keyStream).distinct()
				.toArray(ISparseKey[]::new);
		final Map<ISparseKey, Integer> inverseFeatureMap = new HashMap<>();
		for (int i = 0; i < featureMap.length; i++) {
			inverseFeatureMap.put(featureMap[i], i);
		}

		log.info("Created feature map of size: " + featureMap.length);

		try {
			final IloCplex cplex = new IloCplex();
			final IloNumVar[] weightsPlus = cplex.numVarArray(
					featureMap.length, 0, Double.MAX_VALUE);
			final IloNumVar[] weightsMinus = cplex.numVarArray(
					featureMap.length, 0, Double.MAX_VALUE);

			final IloLinearNumExpr weightsL1 = cplex.linearNumExpr();
			for (int i = 0; i < featureMap.length; i++) {
				weightsL1.addTerm(1.0, weightsPlus[i]);
				weightsL1.addTerm(1.0, weightsMinus[i]);
			}

			final IloNumVar[] upperSlacks = cplex.numVarArray(data.size(), 0,
					Double.MAX_VALUE);
			final IloNumVar[] lowerSlacks = cplex.numVarArray(data.size(), 0,
					Double.MAX_VALUE);
			final IloNumVar biasPlus = cplex.numVar(0, Double.MAX_VALUE);
			final IloNumVar biasMinus = cplex.numVar(0, Double.MAX_VALUE);
			final IloLinearNumExpr slackSum = cplex.linearNumExpr();
			for (int i = 0; i < upperSlacks.length; i++) {
				slackSum.addTerm(c, upperSlacks[i]);
			}
			for (int i = 0; i < lowerSlacks.length; i++) {
				slackSum.addTerm(c, lowerSlacks[i]);
			}
			final IloNumExpr objective = cplex.sum(weightsL1, slackSum);
			cplex.addMinimize(objective);
			for (int i = 0; i < data.size(); i++) {
				final IloLinearNumExpr prediction = cplex.linearNumExpr();
				features.get(i).forEach((key, value) -> {
					final int featureIndex = inverseFeatureMap.get(key);
					try {
						prediction.addTerm(value, weightsPlus[featureIndex]);
						prediction.addTerm(-value, weightsMinus[featureIndex]);
					} catch (final IloException e) {
						throw new RuntimeException(e);
					}
				});
				prediction.addTerm(1, biasPlus);
				prediction.addTerm(-1, biasMinus);
				final IloNumExpr error = cplex.diff(labeler.apply(data.get(i)),
						prediction);
				if (useVariance && varianceLabeler != null) {
					final double customEpsilon = s
							* varianceLabeler.applyAsDouble(data.get(i));
					cplex.addLe(error, cplex.sum(customEpsilon, upperSlacks[i]));
					cplex.addLe(cplex.negative(error),
							cplex.sum(customEpsilon, lowerSlacks[i]));
				} else {
					cplex.addLe(error, cplex.sum(epsilon, upperSlacks[i]));
					cplex.addLe(cplex.negative(error),
							cplex.sum(epsilon, lowerSlacks[i]));
				}
			}
			final boolean success = cplex.solve();
			if (!success) {
				throw new RuntimeException("cplex failed: " + cplex.getStatus());
			}

			final double[] solutionPlus = cplex.getValues(weightsPlus);
			final double[] solutionMinus = cplex.getValues(weightsMinus);
			final double bias = cplex.getValue(biasPlus)
					- cplex.getValue(biasMinus);
			model.getParams().clear();
			for (int i = 0; i < featureMap.length; i++) {
				model.getParams().set(featureMap[i],
						solutionPlus[i] - solutionMinus[i]);
			}
			model.getParams().set(ObjectKey.of(BiasFeatureSet.class), bias);
			cplex.end();
		} catch (final IloException e) {
			throw new RuntimeException(e);
		}
	}

	public static class Creator<V> implements
			IResourceCreator<CustomSVMRegressionLearner<V>> {
		@Override
		public CustomSVMRegressionLearner<V> create(
				HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new CustomSVMRegressionLearner<>(config.getDouble("cost"),
					config.getDouble("epsilon"),
					config.getBoolean("useVariance"));
		}

		@Override
		public String type() {
			return "learner.svm.regression.custom";
		}
	}
}
