package edu.uw.cs.lil.learn.model;

import java.util.List;

import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;
import edu.uw.cs.lil.utils.vector.ISparseVector;

public class LinearBinaryModel<V> implements
		IModel<V, Boolean, IMutableSparseVector> {
	private final LinearContinuousModel<V>	baseModel;

	public LinearBinaryModel(boolean cached) {
		this.baseModel = new LinearContinuousModel<>(cached);
	}

	public LinearBinaryModel(List<IFeatureSet<V>> featureSets, boolean cached) {
		this.baseModel = new LinearContinuousModel<>(featureSets, cached);
	}

	public void addFeatureSet(IFeatureSet<V> fs) {
		this.baseModel.addFeatureSet(fs);
	}

	public LinearContinuousModel<V> getBaseModel() {
		return baseModel;
	}

	@Override
	public ISparseVector getFeatures(V sample) {
		return this.baseModel.getFeatures(sample);
	}

	@Override
	public IMutableSparseVector getParams() {
		return this.baseModel.getParams();
	}

	@Override
	public Boolean getResult(V sample) {
		return baseModel.getResult(sample) > 0;
	}
}
