package edu.uw.cs.lil.learn.model;

import java.util.function.Function;

import edu.uw.cs.lil.utils.vector.ISparseVector;

public class MappedModel<V, A, B, P> implements IModel<V, B, P> {
	private final IModel<V, A, P>	baseModel;
	private final Function<A, B>	resultMapper;

	public MappedModel(IModel<V, A, P> baseModel, Function<A, B> resultMapper) {
		this.baseModel = baseModel;
		this.resultMapper = resultMapper;
	}

	@Override
	public ISparseVector getFeatures(V sample) {
		return baseModel.getFeatures(sample);
	}

	@Override
	public P getParams() {
		return baseModel.getParams();
	}

	@Override
	public B getResult(V sample) {
		return resultMapper.apply(baseModel.getResult(sample));
	}
}
