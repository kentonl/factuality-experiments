package edu.uw.cs.lil.learn.featureset;

import java.util.List;

import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;

public class CompositeFeatureSet<DI> implements IFeatureSet<DI> {
	private final List<IFeatureSet<DI>>	featureSets;

	public CompositeFeatureSet(List<IFeatureSet<DI>> featureSets) {
		this.featureSets = featureSets;
	}

	@Override
	public ISparseVector getFeatures(DI dataItem) {
		return featureSets.stream().map(fs -> fs.getFeatures(dataItem))
				.collect(SparseVector.sumCollector());
	}
}
