package edu.uw.cs.lil.learn.featureset;

import edu.uw.cs.lil.utils.vector.ISparseVector;

public interface IFeatureSet<DI> {
	ISparseVector getFeatures(DI dataItem);
}
